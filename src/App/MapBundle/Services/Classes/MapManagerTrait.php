<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-04-12
 * Time: 10:23
 */

namespace App\MapBundle\Services\Classes;

use Symfony\Component\Cache\Simple\FilesystemCache;

trait MapManagerTrait
{
	protected $apiHandler;
	protected $cache;
	protected $cities;
	
	private function initCityList( $rootDir )
	{
		if(!$this->cache->has( 'csv.miasta-lista.txt' ) )
		{
			$this->cities =  file(
				$rootDir.'/../csv/miasta-lista.txt',
				FILE_IGNORE_NEW_LINES
			);
			$this->cache->set( 'csv.miasta-lista.txt', $this->cities );
		}
		else
		{
			$this->cities = $this->cache->get( 'csv.miasta-lista.txt' );
		}
	}
	
	private function isOnCityList( $cityName )
	{
		return in_array( $cityName, $this->cities );
	}
	
	public function getRecord( &$A, &$B, $record )
	{
		// @todo: usuń z rekordów 'address'y
		return array_merge(
			array_values( $A ),
			array_values( $B ),
			$record
		);
	}
	
}