<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-03-28
 * Time: 09:25
 */

namespace App\MapBundle\Model;

class CsvUploadModel
{
    private $origins;
    private $destinations;
    private $email;

    public function getOrigins()
    {
        return $this->origins;
    }

    public function setOrigins( $origins )
    {
        return $this->origins = $origins;
    }

    public function getDestinations()
    {
        return $this->destinations;
    }

    public function setDestinations( $destinations )
    {
        return $this->destinations = $destinations;
    }
    
    public function getEmail()
	{
		return $this->email;
	}
	
	public function setEmail( $email )
	{
		$this->email = $email;
	}
	
	
	
}