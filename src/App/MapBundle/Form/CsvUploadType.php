<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-03-28
 * Time: 09:12
 */

namespace App\MapBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use App\MapBundle\Model\CsvUploadModel;

class CsvUploadType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add( 'origins', FileType::class )
            ->add( 'destinations', FileType::class );
	
        //
        if( $options['required_email'] )
		{
			$builder
				->add( 'email', EmailType::class );
		}
        
        //
		$builder
        	->add( 'submit', SubmitType::class, [
                    'label' => 'Wyślij'
                ]);
            
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CsvUploadModel::class,
			'required_email' => false,
        ]);
    }

    public function getName()
    {
        return 'csv_upload';
    }
}