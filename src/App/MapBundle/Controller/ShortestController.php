<?php

namespace App\MapBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use App\MapBundle\Model\CsvUploadModel;
use App\MapBundle\Form\CsvUploadType;
use App\MapBundle\Entity\Packet;

class ShortestController extends Controller
{
	/**
	 * @Route("/shortest")
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function indexAction( Request $request )
	{
		set_time_limit ( 0 );
		
		//
		$model = new CsvUploadModel();
		$form = ($this->get('form.factory')->create(
			CsvUploadType::class,
			$model,
			[
				'method' => 'POST',
				'validation_groups' => 'csv_upload',
				'required_email' => true,
			]
		));
		
		//
		$form->handleRequest( $request );
		
		//
		if( $form->isSubmitted() && $form->isValid() )
		{
			$entityManager = $this->getDoctrine()->getManager();
			
			//
			$filePath = $this->get('shortest.manager')->addRequests(
				$model->getEmail(),
				$model->getOrigins()->getPathName(),
				$model->getDestinations()->getPathName()
			);
			
			//
			return $this->render(
				'@AppMap/Default/saved.html.twig',
				[
					'form' => $form->createView(),
				]
			);
		}
		
		return $this->render(
			'@AppMap/Default/index.html.twig',
			[
				'form' => $form->createView(),
			]
		);
	}
}
