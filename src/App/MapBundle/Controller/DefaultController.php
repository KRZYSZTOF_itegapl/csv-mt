<?php

namespace App\MapBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use App\MapBundle\Model\CsvUploadModel;
use App\MapBundle\Form\CsvUploadType;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     *
     * @param Request $request
     * @return Response
     */
    public function indexAction( Request $request )
    {
        $model = new CsvUploadModel();
        $form = ($this->get('form.factory')->create(
                CsvUploadType::class,
                $model,
                [
                    'method' => 'POST',
                    'validation_groups' => 'csv_upload'
                ]
            ));

        //
        $form->handleRequest( $request );

        //
        if( $form->isSubmitted() && $form->isValid() )
        {
            $filePath = $this->get('map.manager')->calculate(
                $model->getOrigins()->getPathName(),
                $model->getDestinations()->getPathName()
            );

            //
            $file = new File( $filePath );
            return $this->file( $file, 'output.csv' );
        }

        return $this->render(
                '@AppMap/Default/index.html.twig',
                [
                    'form' => $form->createView(),
                ]
            );
    }
}
