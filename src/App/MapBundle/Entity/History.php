<?php

namespace App\MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * History
 *
 * @ORM\Table(name="history")
 * @ORM\Entity(repositoryClass="App\MapBundle\Repository\HistoryRepository")
 */
class History
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="originAddress", type="string", length=255)
     */
    private $originAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="destinationAddress", type="string", length=255)
     */
    private $destinationAddress;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="float")
     */
    private $distance;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set originAddress
     *
     * @param string $originAddress
     *
     * @return History
     */
    public function setOriginAddress($originAddress)
    {
        $this->originAddress = $originAddress;

        return $this;
    }

    /**
     * Get originAddress
     *
     * @return string
     */
    public function getOriginAddress()
    {
        return $this->originAddress;
    }

    /**
     * Set destinationAddress
     *
     * @param string $destinationAddress
     *
     * @return History
     */
    public function setDestinationAddress($destinationAddress)
    {
        $this->destinationAddress = $destinationAddress;

        return $this;
    }

    /**
     * Get destinationAddress
     *
     * @return string
     */
    public function getDestinationAddress()
    {
        return $this->destinationAddress;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return History
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return int
     */
    public function getDistance()
    {
        return $this->distance;
    }
}

