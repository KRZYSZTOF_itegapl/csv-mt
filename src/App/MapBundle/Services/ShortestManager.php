<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-04-12
 * Time: 10:19
 */

namespace App\MapBundle\Services;

use App\MapBundle\Entity\History;
use Symfony\Component\Cache\Simple\FilesystemCache;
use DateTime;
use Swift_Attachment;
use Swift_Message;
use App\MapBundle\Services\Classes\MapShortestDistancesClass;
use App\MapBundle\Services\Classes\MapManagerTrait;
use App\MapBundle\Handler\ApiHandler;
use App\MapBundle\Entity\Distance;
use App\MapBundle\Entity\Packet;
use Doctrine\ORM\EntityManager;
use League\Csv\Reader;
use League\Csv\Writer;

class ShortestManager
{
	use MapManagerTrait;
	const LIMIT = 10;
	
	private $em;
	private $persists = 0;
	private $mailer;
	
	public function __construct( $rootDir, $apiHandler, EntityManager $entityManager, $mailer )
	{
		$this->cache = new FilesystemCache();
		$this->apiHandler = $apiHandler;
		$this->em = $entityManager;
		$this->mailer = $mailer;
		$this->initCityList( $rootDir );
	}

	private function addRequestPoint( $packet, $origin, $destination, $x, $y )
	{
		$point = new Distance();
		$point->setDestination( $destination );
		$point->setOrigin( $origin );
		
		//
		$point->setX( $x );
		$point->setY( $y );
		
		//
		$distance = $this->getHistoryDistance(
			$origin['address'],
			$destination['address']
		);
		
		//
		if( $distance )
		{
			$point->setCalculated(true);
			$point->setDistance($distance);
		}
		
		//
		$packet->addDistance( $point );
		
		//
		$this->em->persist( $point );
		$this->persists++;
		
		//
		if( $this->persists > 1000 )
		{
			$this->em->flush();
			$packet->clearDistances();
			$this->persists = 0;
		}
	}
	
	public function addRequests( $email, $origins, $destinations )
	{
		$packet = new Packet();
		$packet->setEmail( $email );
		$packet->setCompleted( true );
		$this->em->persist( $packet );
		$this->em->flush();
		
		//
		$origins = Reader::createFromPath( $origins, 'r' );
		$destinations = Reader::createFromPath( $destinations, 'r' );
		
		//
		$origins->setDelimiter(';');
		$destinations->setDelimiter(';');
		
		//
		$originsRecords = $origins->getRecords([ 'street', 'postCode', 'cityName', 'name', 'surname' ]);
		$destinationsRecords = $destinations->getRecords([ 'postCode', 'cityName', 'name' ]);
		
		//
		$x = 0;
		foreach( $destinationsRecords as $destination )
		{
			$y = 0;
			
			//
			foreach( $originsRecords as $origin )
			{
				if( $x%self::LIMIT == $y%self::LIMIT )
				{
					$destination['address'] = $this->apiHandler->getLocation(
							$destination['cityName'],
							(($this->isOnCityList($destination['cityName'])) ? '' : $destination['postCode'])
						);
					
					//
					$origin['address'] = $this->apiHandler->getLocation(
							$origin['cityName'],
							$origin['postCode'],
							$origin['street']
						);

					//
					$this->addRequestPoint(
							$packet,
							$origin,
							$destination,
							$x, $y
						);
				}
				
				//
				$y++;
			}
			
			//
			$x++;
		}
		
		//
		$x = 0;
		foreach( $destinationsRecords as $destination )
		{
			$y = 0;

			//
			$destination['address'] = $this->apiHandler->getLocation(
				$destination['cityName'],
				(($this->isOnCityList($destination['cityName'])) ? '' : $destination['postCode'])
			);
			
			//
			foreach( $originsRecords as $origin )
			{
				if(!( $x%self::LIMIT == $y%self::LIMIT ))
				{
					$origin['address'] = $this->apiHandler->getLocation(
							$origin['cityName'],
							$origin['postCode'],
							$origin['street']
						);
					
					//
					$this->addRequestPoint(
						$packet,
						$origin,
						$destination,
						$x, $y
					);
				}
				
				//
				$y++;
			}
			
			//
			$x++;
		}

		//
		$packet->setCompleted( false );
		$this->em->flush();
	}

	//
	public function setHistoryDistance( $addressOrigin, $addressDestination, $distance )
	{
		$history = new History();
		$history->setOriginAddress( $addressOrigin );
		$history->setDestinationAddress( $addressDestination );
		$history->setDistance( $distance );
		$this->em->persist( $history );
	}
	
	public function getHistoryDistance( $addressOrigin, $addressDestination )
	{
		$point = $this->em->getRepository( History::class )
			->getPoint(
					$addressOrigin, $addressDestination
				);

		//
		if( $point )
			return $point->getDistance();
		
		return null;
	}
	
	//
	public function getCurrentPacket()
	{
		return $this->em->getRepository( Packet::class )->getCurrent();
	}
	
	public function getCurrentRecords( $packetId, $limit = 100 )
	{
		return $this->em->getRepository(Distance::class)->getPoints(
				$packetId,
				$limit,
				true
			);
	}
	
	//
	public function generateOutputFile( $packetId )
	{
		$filePath = tempnam( sys_get_temp_dir(), 'map.csv' );
		$output = Writer::createFromPath( $filePath, 'w+' );
		$output->setDelimiter(';');
		
		//
		$distancesRepository = $this->em->getRepository( Distance::class );
		$distancesGroups = $distancesRepository->getShortestGroups( $packetId );
		
		//
		$output->insertOne([]);
		$output->insertOne(['#### Najkrótsze trasy ####']);
		$output->insertOne(['SKLEP_KOD_POCZTWOY',
							'SKLEP_MIASTO',
							'SKLEP_NAZWA',
							'ANKIETER1',
							'DOJAZD_ANKIETER1',
							'ANKIETER2',
							'DOJAZD_ANKIETER2',
							'ANKIETER3',
							'DOJAZD_ANKIETER3'
		]);
		
		//
		foreach( $distancesGroups as $result )
		{
			$distancesShortest = $distancesRepository->getShortestDistances( $packetId, $result['x'] );
			
			// wyciagamy dane sklepu
			if( $distancesShortest )
			{
				$destination = $distancesShortest[0]->getDestination();
			}
			else
			{
				// niezlokalizowane lokacje
				$destination = $distancesRepository->findBy([
						'packet' => $packetId,
						'x' => $result['x']
					])[0]->getDestination();
			}
			
			//
			$record = [
					$destination['postCode'],
					$destination['cityName'],
					$destination['name']
				];
			
			// wypisanie wyników
			if( $distancesShortest )
			{
				foreach( $distancesShortest as $distance )
				{
					$origin = $distance->getOrigin();
					array_push( $record,
							$origin['name'].' '.$origin['surname'],
							$distance->getDistance()
						);
				}
			}
			
			//
			$output->insertOne($record);
		}
		
		//
		return $filePath;
	}
	
	public function sendResult( Packet $packet )
	{
		$message = \Swift_Message::newInstance()
			->setSubject('Najkrótsze trasy')
			->setFrom( 'informacje@trasa.abrsesta.com' )
			->setTo( $packet->getEmail() );

		//
		$message->attach(
				Swift_Attachment::fromPath( $this->generateOutputFile( $packet->getId() ) )
					->setFilename( 'output.csv' )
			);
		
		//
		if( $this->mailer->send($message) )
		{
			$packet->setSended( true );
			$packet->setDatetime( new \DateTime('now') );
		}
	}
	
	//
	public function calculate( $requestsLimit = 10 )
	{
		$packet = $this->getCurrentPacket();
		
		//
		if(!$packet )
		{
			echo( 'all distances are calculated');
			exit();
		}
		
		//
		$records = [];
		while( $requestsLimit && $records = $this->getCurrentRecords( $packet->getId(), self::LIMIT ) )
		{
			$addressesDestinations = [];
			$addressesOrigins = [];
			
			foreach( $records as $record )
			{
				$destination = $record->getDestination();
				$origin = $record->getOrigin();
					
				//
				if(!$this->apiHandler->getDistance(
					$origin['address'], $destination['address'] )
				)
				{
					$inDestinations = in_array( $destination['address'], $addressesDestinations );
					$inOrigins = in_array( $origin['address'], $addressesOrigins );
					
					//
					if(!$inDestinations )
					{
						array_push( $addressesDestinations, $destination['address'] );
					}
						
					if(!$inOrigins )
					{
						array_push( $addressesOrigins, $origin['address'] );
					}
				}
			}
			
			//
			if( $addressesOrigins && $addressesDestinations )
			{
				$this->apiHandler->getDistances(
					$addressesOrigins,
					$addressesDestinations
				);
			}
			
			//
			foreach( $records as $record )
			{
				$destination = $record->getDestination();
				$origin = $record->getOrigin();
			
				//
				$distance = $this->apiHandler->getDistance(
						$origin['address'],
						$destination['address']
					);
					
				//
				$record->setCalculated( true );
				
				//
				if( $distance )
				{
					$record->setDistance( $distance );
					$this->setHistoryDistance(
						$origin['address'],
						$destination['address'],
						$distance
					);
				}
			}

			//
			$this->em->flush();
			$requestsLimit--;
		}
		
		//
		if( !$records )
		{
			$this->sendResult( $packet );
			$packet->setCompleted( true );
			$this->em->flush();
			echo 'Done';
		}
		
	}
}