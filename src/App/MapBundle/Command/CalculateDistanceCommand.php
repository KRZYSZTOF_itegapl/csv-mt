<?php
namespace App\MapBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateDistanceCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('calculate:distance')
			->setDescription('calculate distances')
			->addArgument(
				'limit',
				InputArgument::REQUIRED,
				'requests limit'
			);
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$services = $this->getContainer();
		$shortestManager = $services->get( 'shortest.manager' );
		$shortestManager->calculate( $input->getArgument('limit') );
	}
}