<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-03-30
 * Time: 11:49
 */
namespace App\MapBundle\Services\Classes;

class MapShortestDistancesClass
{
	protected $count;
	
	/**
	 * @var array
	 *
	 * Rekord ma postać [ $distance, $record ]
	 * Pierszy element tablicy $records jest najkrótszy
	 * Tablica jest uporządkowana od najkródszej trasy
	 *
	 */
	protected $records = [];
	
	//
	public function __construct( $size )
	{
		$this->count = $size;
	}
	
	//
	public function setRecord( $distance, $record )
	{
		$position = $size = count( $this->records );
		
		//
		for( $i = $size-1; $i >=0; $i-- )
		{
			if( $this->records[$i][0] > $distance )
			{
				$position = $i;
			}
		}
		
		//
		if( $position < $size )
		{
			if( $size >= $this->count )
			{
				array_pop($this->records);
			}
			array_splice( $this->records, $position, 0, [[ $distance, $record ]] );
			return;
		}
		
		//
		if( $size < $this->count )
		{
			array_push( $this->records, [ $distance, $record ] );
		}
	}
	
	//
	public function getRecords()
	{
		$output = [];
		
		//
		foreach( $this->records as $record )
		{
			$output[] = $record[1];
		}
		
		//
		return $output;
	}
}