<?php

namespace App\MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Distance
 *
 * @ORM\Table(name="distance")
 * @ORM\Entity(repositoryClass="App\MapBundle\Repository\DistanceRepository")
 */
class Distance
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var array
     *
     * @ORM\Column(name="origin", type="array")
     */
    private $origin;

    /**
     * @var array
     *
     * @ORM\Column(name="destination", type="array")
     */
    private $destination;

    /**
     * @var bool
     *
     * @ORM\Column(name="calculated", type="boolean")
     */
    private $calculated = false;

    /**
     * @var int
     *
     * @ORM\Column(name="distance", type="float", nullable=true)
     */
    private $distance;
	
	/**
	 * @var int
	 *
	 * @ORM\Column(name="x", type="integer")
	 */
	private $x = 0;
	
	/**
	 * @var int
	 *
	 * @ORM\Column(name="y", type="integer")
	 */
	private $y = 0;
	
	
	/**
	 * @ORM\ManyToOne(targetEntity="Packet", inversedBy="distances")
	 * @ORM\JoinColumn(name="packet", referencedColumnName="id")
	 */
    private $packet;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set origin
     *
     * @param array $origin
     *
     * @return Distance
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;

        return $this;
    }

    /**
     * Get origin
     *
     * @return array
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * Set destination
     *
     * @param array $destination
     *
     * @return Distance
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return array
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * Set calculated
     *
     * @param boolean $calculated
     *
     * @return Distance
     */
    public function setCalculated($calculated)
    {
        $this->calculated = $calculated;

        return $this;
    }

    /**
     * Get calculated
     *
     * @return bool
     */
    public function getCalculated()
    {
        return $this->calculated;
    }

    /**
     * Set distance
     *
     * @param integer $distance
     *
     * @return Distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Get distance
     *
     * @return int
     */
    public function getDistance()
    {
        return $this->distance;
    }
    
	public function setPacket( $packet )
	{
		$this->packet = $packet;

		return $this;
	}
	
	public function getPacket()
	{
		return $this->packet;
	}
	
	public function setX( $x )
	{
		$this->x = $x;
		return $this;
	}
	
	public function getX()
	{
		return $this->x;
	}
	
	public function setY( $y )
	
	
	
	
	{
		$this->y = $y;
		return $this;
	}
	
	public function getY()
	{
		return $this->y;
	}
	
}

