<?php

namespace App\MapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\MapBundle\Entity\Distance;


/**
 * Packet
 *
 * @ORM\Table(name="packet")
 * @ORM\Entity(repositoryClass="App\MapBundle\Repository\PacketRepository")
 * @todo: add index for history :)
 */
class Packet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var bool
     *
     * @ORM\Column(name="completed", type="boolean")
     */
    private $completed = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="sended", type="boolean")
     */
    private $sended = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=true)
     */
    private $datetime;
	
	
	/**
	 * @ORM\OneToMany(targetEntity="Distance", mappedBy="packet", fetch="EXTRA_LAZY" )
	 */
    private $distances;
    
    public function __construct()
	{
		$this->distances = new ArrayCollection();
	}
	
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Packet
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set completed
     *
     * @param boolean $completed
     *
     * @return Packet
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * Get completed
     *
     * @return bool
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * Set sended
     *
     * @param boolean $sended
     *
     * @return Packet
     */
    public function setSended($sended)
    {
        $this->sended = $sended;

        return $this;
    }

    /**
     * Get sended
     *
     * @return bool
     */
    public function getSended()
    {
        return $this->sended;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     *
     * @return Packet
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }
    
    /**
     * Add distance
     *
     * @param Distance $distance
     *
     * @return Packet
     */
    public function addDistance(Distance $distance)
    {
        $this->distances[] = $distance;
		
        // @todo: czy to powinno być tutaj [..]?
		$distance->setPacket( $this );

		return $this;
    }

    /**
     * Remove distance
     *
     * @param Distance $distance
     */
    public function removeDistance(Distance $distance)
    {
        $this->distances->removeElement($distance);
    }

    /**
     * Get distances
     *
     * @return ArrayCollection
     */
    public function getDistances()
    {
        return $this->distances;
    }
    
    public function clearDistances()
	{
		$this->distances = new ArrayCollection();
		return $this;
	}

}
