<?php
namespace App\MapBundle\Handler;
use Symfony\Component\Cache\Simple\FilesystemCache;

class ApiHandler
{
	protected $cache;
	// @todo? jak czyścić cache
	const CACHE_KEY = 'map.api-dm.data';
	protected $cachedData = [];
	protected $apiKey;
	protected $log;
	
	public function __construct( string $apiKey, $log )
	{
		$this->apiKey = $apiKey;
		$this->cache = new FilesystemCache();
		$this->log = $log;
		
		//
		if( $this->cache->has( self::CACHE_KEY ) )
		{
			$this->cachedData = $this->cache->get( self::CACHE_KEY );
		}
	}
	
	public function __destruct()
	{
		$this->cache->set( self::CACHE_KEY, $this->cachedData );
	}
	
	protected function getCurl( string $address, array $get = [] )
	{
		$url = $address;
		
		//
		if( $get )
		{
			// @todo: generate and add here api key usage?
			$url .= '?'.http_build_query( $get );
		}
		
		//
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		curl_close($ch);
		
		//
		return json_decode($response, true);
	}
	
	public function getLocation( string $cityName,
								 string $postCode = '',
								 string $street = ''
	)
	{
		$address = '';
		if( $street ) $address .= $street.' ';
		$address .= $cityName;
		if( $postCode ) $address .= ' '.$postCode;
		
		return $address;
	}
	
	public function getDistances( array $addressesOrigins, array $addressesDestinations )
	{
		$get = [
				'origins' => join( '|', $addressesOrigins ),
				'destinations' => join( '|', $addressesDestinations ),
				'mode' => 'driving'
			];
		
		//
		if( $this->apiKey )
		{
			$get['key'] = $this->apiKey;
		}
		
		//
		$json = $this->getCurl('https://maps.googleapis.com/maps/api/distancematrix/json', $get );
		
		//
		if( $json['status'] == 'OK' )
		{
			$originsCount = count( $addressesOrigins );
			$destinationsCount = count( $addressesDestinations );
			
			for( $originsIndex = 0; $originsIndex < $originsCount; $originsIndex++ )
			{
				for( $destinationsIndex = 0; $destinationsIndex < $destinationsCount; $destinationsIndex++ )
				{
					if( $json['rows'][$originsIndex]['elements'][$destinationsIndex]['status'] == 'OK' )
					{
						$this->setDistance(
							$addressesOrigins[ $originsIndex ],
							$addressesDestinations[ $destinationsIndex ],
							$json['rows'][$originsIndex]['elements'][$destinationsIndex]['distance']['value']/1000
						);
					}
					else
					{
						$this->setDistance(
							$addressesOrigins[ $originsIndex ],
							$addressesDestinations[ $destinationsIndex ],
							false
						);
					}
				}
			}
		}
		else
		{
			dump( 'https://maps.googleapis.com/maps/api/distancematrix/json?'.http_build_query( $get ) );
			$this->log->critical( 'RESULT:', $json );
			exit();
		}
	}
	
	public function getDistance( $addressOrigin, $addressDestination )
	{
		if(!isset( $this->cachedData[ $addressOrigin ] ) )
		{
			return null;
		}
		
		if(!isset( $this->cachedData[ $addressOrigin ][ $addressDestination ] ) )
		{
			return null;
		}
		
		//
		return $this->cachedData[ $addressOrigin ][ $addressDestination ];
	}
	
	public function setDistance( $addressOrigin, $addressDestination, $distance )
	{
		if(!isset( $this->cachedData[ $addressOrigin ] ) )
		{
			$this->cachedData[ $addressOrigin ] = [];
		}
		
		//
		$this->cachedData[ $addressOrigin ][ $addressDestination ] = $distance;
	}
	
}