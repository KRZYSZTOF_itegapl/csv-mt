<?php
/**
 * Created by PhpStorm.
 * User: KRZYSZTOF
 * Date: 2018-03-27
 * Time: 12:59
 */
namespace App\MapBundle\Services;

use Symfony\Component\Cache\Simple\FilesystemCache;
use App\MapBundle\Services\Classes\MapManagerTrait;
use League\Csv\Reader;
use League\Csv\Writer;


class MapManager
{
	use MapManagerTrait;
	
	public function __construct( $rootDir, $apiHandler )
	{
		$this->cache = new FilesystemCache();
		$this->apiHandler = $apiHandler;
		
		//
		$this->initCityList( $rootDir );
	}
	
    public function calculate( $origins, $destinations )
    {
        $filePath = tempnam( sys_get_temp_dir(), 'map.csv' );

        //
        $origins = Reader::createFromPath( $origins, 'r' );
        $destinations = Reader::createFromPath( $destinations, 'r' );
        $output = Writer::createFromPath( $filePath, 'w+' );

        //
        $origins->setDelimiter(';');
        $destinations->setDelimiter(';');
        $output->setDelimiter(';');

        //
        $originsRecords = $origins->getRecords([ 'street', 'postCode', 'cityName', 'name', 'surname' ]);
        $destinationsRecords = $destinations->getRecords([ 'postCode', 'cityName', 'name' ]);

        //
        $destinationsRecords->rewind();
        foreach( $originsRecords as $origin )
        {
            $destination = $destinationsRecords->current();
            $destinationsRecords->next();

            //
            $originLocation = $this->apiHandler->getLocation(
                    $origin['cityName'],
                    $origin['postCode'],
                    $origin['street']
                );

            //
            if( $originLocation )
            {
                $destinationLocation = $this->apiHandler->getLocation(
                    $destination['cityName'],
                    (($this->isOnCityList($destination['cityName'])) ? '' : $destination['postCode'])
                );

                if( $destinationLocation )
                {
                	$this->apiHandler->getDistances( [$originLocation], [$destinationLocation] );
                    $distance = $this->apiHandler->getDistance( $originLocation, $destinationLocation );

                    //
                    $output->insertOne( $this->getRecord(
							$origin,
							$destination,
							[$distance]
						));

                    //
                    continue;
                }
            }

            //
            $output->insertOne($this->getRecord(
					$origin,
					$destination,
					['#BRAK DANYCH#']
				));
        }

        //
        return $filePath;
    }
}